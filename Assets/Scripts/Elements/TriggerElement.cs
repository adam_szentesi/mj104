using System.Collections.Generic;
using UnityEngine;

public class TriggerElement : Element
{
  [SerializeField]
  private List<Element> ElementsToTrigger = new List<Element>();

  [SerializeField]
  private GameObject LineRendererPrefab;

  private List<LineRenderer> LineRenderers = new List<LineRenderer>();

  private void Start()
  {
    foreach (Element element in ElementsToTrigger)
    {
      if (element != null)
      {
        LineRenderer lineRenderer = Instantiate(LineRendererPrefab, transform).GetComponent<LineRenderer>();
        LineRenderers.Add(lineRenderer);
        lineRenderer.SetPositions(new Vector3[] { transform.position, element.transform.position });
        lineRenderer.gameObject.SetActive(true);
      }
    }
  }

  private void OnDestroy()
  {
    foreach (LineRenderer lineRenderer in LineRenderers)
    {
      Destroy(lineRenderer.gameObject);
    }
  }

  protected void Trigger()
  {
    foreach (Element element in ElementsToTrigger)
    {
      if (element != null)
      {
        element.Activate();
      }
    }
  }

  private void OnDrawGizmos()
  {
    Gizmos.color = new Color(0, 0, 0, 1);

    foreach (Element element in ElementsToTrigger)
    {
      if (element != null)
      {
        Gizmos.DrawLine(transform.position, element.transform.position);
      }
    }
  }

}
