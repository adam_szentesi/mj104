using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Domino/SingleDominoDeployer")]
public class SingleDominoDeployer : DominoDeployer
{
  public override int Deploy(Car car, Action SpawnDomino)
  {
    SpawnDomino.Invoke();
    return 1;
  }

  public override void Init()
  {
    throw new NotImplementedException();
  }
}
