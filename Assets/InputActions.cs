// GENERATED AUTOMATICALLY FROM 'Assets/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""a5b22d86-05e3-4516-a1e0-8594cad3651c"",
            ""actions"": [
                {
                    ""name"": ""Forward"",
                    ""type"": ""Value"",
                    ""id"": ""8f87f5f4-3854-4f51-bbc6-4d95e03a0a47"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Value"",
                    ""id"": ""99a24c5b-baa3-4980-9a1e-fc592d728ac5"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Start"",
                    ""type"": ""Button"",
                    ""id"": ""c2ecf242-0935-44ee-a17a-a6f0e991b8d5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PreviousCar"",
                    ""type"": ""Button"",
                    ""id"": ""5b460476-a2f7-45a7-acc3-32739a1619f6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""NextCar"",
                    ""type"": ""Button"",
                    ""id"": ""085fa025-3d65-4ecb-ac46-134703ab91c0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Turn"",
                    ""type"": ""Value"",
                    ""id"": ""44d8973b-bb8d-4b82-9bdc-f629de54930a"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Gas"",
                    ""type"": ""Value"",
                    ""id"": ""382aa30f-e514-4ed7-b66d-2cc733b82af3"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Undo"",
                    ""type"": ""Button"",
                    ""id"": ""47fe3d1d-8632-4c4c-b6b7-8acc0624dd56"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""aae72f1c-d38d-4190-a1d5-6b91d203af16"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""35775168-3621-46bb-9b01-6920d57a39cb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4949ef0c-3269-4ac7-a22c-ee0de312753f"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""f649d463-9bfc-4679-8082-d56b134c4324"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Forward"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8cba3a26-9034-4a3b-a92b-7eabda00e9e5"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7cb962c5-32da-4b25-9d56-eed606446ecd"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b9f98ec0-daec-42f5-8644-cf89e55f9848"",
                    ""path"": ""<Gamepad>/leftStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""4bb2fe99-19c6-4bee-9535-f535f4828486"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""67b29a5d-b294-4f46-801c-89217525aabe"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""f942b764-9395-4583-b878-fe173d011a28"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""e6b7f92c-7486-4d1e-a70c-3e1150f1880c"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48c49439-bcd3-4551-bc4f-a5af71fb1221"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2eaad3ab-7327-422a-9820-6bde2cf97d9e"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PreviousCar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9e3bd23-18cd-41f9-a1cc-6fae5dff0fed"",
                    ""path"": ""<Keyboard>/pageDown"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PreviousCar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dc5cff44-c5de-4a44-b775-fcc774a1fce3"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextCar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb970c63-d3e3-4790-bb0e-90aa9e827b43"",
                    ""path"": ""<Keyboard>/pageUp"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextCar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b1fcd167-7b1e-4bf1-80ab-d04a21bf68fa"",
                    ""path"": ""<Gamepad>/rightStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""ab8f5f79-0e0d-4d8b-b1ab-f73def55a114"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""00204169-f85a-4cfb-b0bf-ceb1ab6dbab2"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""523be7d2-91da-4079-b6aa-fd61d3e6c5ee"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""443ead97-88e6-42a2-93e0-8e89281b05a8"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Gas"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4fe2f5f8-40b3-4c79-a8d2-fe66e08b9ba5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Gas"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef91b6ee-fb78-4e7a-8a2a-88825f98bcbc"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Undo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""93f881ac-da18-46d4-bc8a-94f07ccb5b69"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Undo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9d64aeaf-c934-44fc-ba1a-c4a37ed8a1a3"",
                    ""path"": ""<Keyboard>/backspace"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f1ba4614-423b-48ad-a84a-aa7d06220b9e"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f65d691c-ff6d-4ff1-ad0b-c7911f522888"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c6f5149a-bed5-438b-a70a-cf542ea53efd"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""BranchPanel"",
            ""id"": ""60969178-ba3d-421f-9433-67ca2b19eeb6"",
            ""actions"": [
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""76631a5c-53ed-4cca-b041-17e664922a46"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""8518d83a-0c23-426d-bd6d-55bfc115b155"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""0a78d7ea-f6a7-4fee-844c-3d16e728c7e9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""10270bb3-4fa9-4654-8b10-32cfad1f7f65"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""02082ed9-efea-43e0-91db-94c9a3439fbb"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fb702c8b-a749-4de8-89b5-056bab3f3a5e"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""09b6738b-79e4-43fb-8703-8ca4dbe002f5"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ebd77bd1-dc18-40f5-b4e9-cba0b9faee03"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bca2005d-86e0-4018-89b5-0203cef42f4c"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""DialoguePanel"",
            ""id"": ""2e7503f8-3f54-4d67-9ac9-e4ce75a2cb16"",
            ""actions"": [
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""62bdef5e-6c54-4605-b732-c8e55d9ecaaa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""5d238493-9508-448b-9880-922c37b39b39"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f0ff356b-e37d-4857-a97a-030a9c55d4a9"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""55bbcfd9-cebb-4341-b10b-d1562e0daf86"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f59012cf-672c-4023-b8b5-43eff20d3806"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""26768dfe-facb-4f37-977a-a89dae6f99de"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MainMenu"",
            ""id"": ""27b91824-55c1-475c-a938-851cf7dbd70c"",
            ""actions"": [
                {
                    ""name"": ""Up"",
                    ""type"": ""Button"",
                    ""id"": ""925da479-dede-4739-baea-4500b58d6815"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""b2fadf0d-b912-462b-a0e8-eb2580eae54d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""459343ed-9397-49e4-9fe3-186b0da2d5bc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reset"",
                    ""type"": ""Button"",
                    ""id"": ""5647805c-99d3-45d8-963c-f8cddaa496f2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6a7860f8-a2b9-453d-b6b5-cb024891acf6"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fa7c9f63-c878-47d4-8b5d-c9b56317a0c9"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aedd7cbe-93f5-4a7e-afe5-a44a8fb982f8"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0610a70c-a4a9-4375-8ce9-da3d3e90d646"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ff21e1dd-46a2-4f75-89b9-83c07cf2a923"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""626dc1f7-c8d6-4ae3-87ad-0bfcb69027e7"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""70f5206c-0b42-4bd5-ab2f-c2eb6d7faa8d"",
                    ""path"": ""<Keyboard>/delete"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_Forward = m_Gameplay.FindAction("Forward", throwIfNotFound: true);
        m_Gameplay_Right = m_Gameplay.FindAction("Right", throwIfNotFound: true);
        m_Gameplay_Start = m_Gameplay.FindAction("Start", throwIfNotFound: true);
        m_Gameplay_PreviousCar = m_Gameplay.FindAction("PreviousCar", throwIfNotFound: true);
        m_Gameplay_NextCar = m_Gameplay.FindAction("NextCar", throwIfNotFound: true);
        m_Gameplay_Turn = m_Gameplay.FindAction("Turn", throwIfNotFound: true);
        m_Gameplay_Gas = m_Gameplay.FindAction("Gas", throwIfNotFound: true);
        m_Gameplay_Undo = m_Gameplay.FindAction("Undo", throwIfNotFound: true);
        m_Gameplay_Restart = m_Gameplay.FindAction("Restart", throwIfNotFound: true);
        m_Gameplay_Exit = m_Gameplay.FindAction("Exit", throwIfNotFound: true);
        // BranchPanel
        m_BranchPanel = asset.FindActionMap("BranchPanel", throwIfNotFound: true);
        m_BranchPanel_Left = m_BranchPanel.FindAction("Left", throwIfNotFound: true);
        m_BranchPanel_Right = m_BranchPanel.FindAction("Right", throwIfNotFound: true);
        m_BranchPanel_Confirm = m_BranchPanel.FindAction("Confirm", throwIfNotFound: true);
        // DialoguePanel
        m_DialoguePanel = asset.FindActionMap("DialoguePanel", throwIfNotFound: true);
        m_DialoguePanel_Confirm = m_DialoguePanel.FindAction("Confirm", throwIfNotFound: true);
        m_DialoguePanel_Cancel = m_DialoguePanel.FindAction("Cancel", throwIfNotFound: true);
        // MainMenu
        m_MainMenu = asset.FindActionMap("MainMenu", throwIfNotFound: true);
        m_MainMenu_Up = m_MainMenu.FindAction("Up", throwIfNotFound: true);
        m_MainMenu_Down = m_MainMenu.FindAction("Down", throwIfNotFound: true);
        m_MainMenu_Confirm = m_MainMenu.FindAction("Confirm", throwIfNotFound: true);
        m_MainMenu_Reset = m_MainMenu.FindAction("Reset", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_Forward;
    private readonly InputAction m_Gameplay_Right;
    private readonly InputAction m_Gameplay_Start;
    private readonly InputAction m_Gameplay_PreviousCar;
    private readonly InputAction m_Gameplay_NextCar;
    private readonly InputAction m_Gameplay_Turn;
    private readonly InputAction m_Gameplay_Gas;
    private readonly InputAction m_Gameplay_Undo;
    private readonly InputAction m_Gameplay_Restart;
    private readonly InputAction m_Gameplay_Exit;
    public struct GameplayActions
    {
        private @InputActions m_Wrapper;
        public GameplayActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Forward => m_Wrapper.m_Gameplay_Forward;
        public InputAction @Right => m_Wrapper.m_Gameplay_Right;
        public InputAction @Start => m_Wrapper.m_Gameplay_Start;
        public InputAction @PreviousCar => m_Wrapper.m_Gameplay_PreviousCar;
        public InputAction @NextCar => m_Wrapper.m_Gameplay_NextCar;
        public InputAction @Turn => m_Wrapper.m_Gameplay_Turn;
        public InputAction @Gas => m_Wrapper.m_Gameplay_Gas;
        public InputAction @Undo => m_Wrapper.m_Gameplay_Undo;
        public InputAction @Restart => m_Wrapper.m_Gameplay_Restart;
        public InputAction @Exit => m_Wrapper.m_Gameplay_Exit;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @Forward.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnForward;
                @Forward.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnForward;
                @Forward.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnForward;
                @Right.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRight;
                @Start.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStart;
                @Start.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStart;
                @Start.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStart;
                @PreviousCar.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousCar;
                @PreviousCar.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousCar;
                @PreviousCar.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousCar;
                @NextCar.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextCar;
                @NextCar.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextCar;
                @NextCar.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextCar;
                @Turn.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                @Turn.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                @Turn.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                @Gas.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGas;
                @Gas.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGas;
                @Gas.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGas;
                @Undo.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUndo;
                @Undo.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUndo;
                @Undo.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUndo;
                @Restart.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestart;
                @Restart.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestart;
                @Restart.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestart;
                @Exit.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnExit;
                @Exit.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnExit;
                @Exit.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnExit;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Forward.started += instance.OnForward;
                @Forward.performed += instance.OnForward;
                @Forward.canceled += instance.OnForward;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Start.started += instance.OnStart;
                @Start.performed += instance.OnStart;
                @Start.canceled += instance.OnStart;
                @PreviousCar.started += instance.OnPreviousCar;
                @PreviousCar.performed += instance.OnPreviousCar;
                @PreviousCar.canceled += instance.OnPreviousCar;
                @NextCar.started += instance.OnNextCar;
                @NextCar.performed += instance.OnNextCar;
                @NextCar.canceled += instance.OnNextCar;
                @Turn.started += instance.OnTurn;
                @Turn.performed += instance.OnTurn;
                @Turn.canceled += instance.OnTurn;
                @Gas.started += instance.OnGas;
                @Gas.performed += instance.OnGas;
                @Gas.canceled += instance.OnGas;
                @Undo.started += instance.OnUndo;
                @Undo.performed += instance.OnUndo;
                @Undo.canceled += instance.OnUndo;
                @Restart.started += instance.OnRestart;
                @Restart.performed += instance.OnRestart;
                @Restart.canceled += instance.OnRestart;
                @Exit.started += instance.OnExit;
                @Exit.performed += instance.OnExit;
                @Exit.canceled += instance.OnExit;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);

    // BranchPanel
    private readonly InputActionMap m_BranchPanel;
    private IBranchPanelActions m_BranchPanelActionsCallbackInterface;
    private readonly InputAction m_BranchPanel_Left;
    private readonly InputAction m_BranchPanel_Right;
    private readonly InputAction m_BranchPanel_Confirm;
    public struct BranchPanelActions
    {
        private @InputActions m_Wrapper;
        public BranchPanelActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Left => m_Wrapper.m_BranchPanel_Left;
        public InputAction @Right => m_Wrapper.m_BranchPanel_Right;
        public InputAction @Confirm => m_Wrapper.m_BranchPanel_Confirm;
        public InputActionMap Get() { return m_Wrapper.m_BranchPanel; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BranchPanelActions set) { return set.Get(); }
        public void SetCallbacks(IBranchPanelActions instance)
        {
            if (m_Wrapper.m_BranchPanelActionsCallbackInterface != null)
            {
                @Left.started -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnLeft;
                @Right.started -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnRight;
                @Confirm.started -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnConfirm;
                @Confirm.performed -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnConfirm;
                @Confirm.canceled -= m_Wrapper.m_BranchPanelActionsCallbackInterface.OnConfirm;
            }
            m_Wrapper.m_BranchPanelActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Confirm.started += instance.OnConfirm;
                @Confirm.performed += instance.OnConfirm;
                @Confirm.canceled += instance.OnConfirm;
            }
        }
    }
    public BranchPanelActions @BranchPanel => new BranchPanelActions(this);

    // DialoguePanel
    private readonly InputActionMap m_DialoguePanel;
    private IDialoguePanelActions m_DialoguePanelActionsCallbackInterface;
    private readonly InputAction m_DialoguePanel_Confirm;
    private readonly InputAction m_DialoguePanel_Cancel;
    public struct DialoguePanelActions
    {
        private @InputActions m_Wrapper;
        public DialoguePanelActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Confirm => m_Wrapper.m_DialoguePanel_Confirm;
        public InputAction @Cancel => m_Wrapper.m_DialoguePanel_Cancel;
        public InputActionMap Get() { return m_Wrapper.m_DialoguePanel; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DialoguePanelActions set) { return set.Get(); }
        public void SetCallbacks(IDialoguePanelActions instance)
        {
            if (m_Wrapper.m_DialoguePanelActionsCallbackInterface != null)
            {
                @Confirm.started -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnConfirm;
                @Confirm.performed -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnConfirm;
                @Confirm.canceled -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnConfirm;
                @Cancel.started -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_DialoguePanelActionsCallbackInterface.OnCancel;
            }
            m_Wrapper.m_DialoguePanelActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Confirm.started += instance.OnConfirm;
                @Confirm.performed += instance.OnConfirm;
                @Confirm.canceled += instance.OnConfirm;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
            }
        }
    }
    public DialoguePanelActions @DialoguePanel => new DialoguePanelActions(this);

    // MainMenu
    private readonly InputActionMap m_MainMenu;
    private IMainMenuActions m_MainMenuActionsCallbackInterface;
    private readonly InputAction m_MainMenu_Up;
    private readonly InputAction m_MainMenu_Down;
    private readonly InputAction m_MainMenu_Confirm;
    private readonly InputAction m_MainMenu_Reset;
    public struct MainMenuActions
    {
        private @InputActions m_Wrapper;
        public MainMenuActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Up => m_Wrapper.m_MainMenu_Up;
        public InputAction @Down => m_Wrapper.m_MainMenu_Down;
        public InputAction @Confirm => m_Wrapper.m_MainMenu_Confirm;
        public InputAction @Reset => m_Wrapper.m_MainMenu_Reset;
        public InputActionMap Get() { return m_Wrapper.m_MainMenu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MainMenuActions set) { return set.Get(); }
        public void SetCallbacks(IMainMenuActions instance)
        {
            if (m_Wrapper.m_MainMenuActionsCallbackInterface != null)
            {
                @Up.started -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnUp;
                @Up.performed -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnUp;
                @Up.canceled -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnUp;
                @Down.started -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnDown;
                @Confirm.started -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnConfirm;
                @Confirm.performed -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnConfirm;
                @Confirm.canceled -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnConfirm;
                @Reset.started -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnReset;
                @Reset.performed -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnReset;
                @Reset.canceled -= m_Wrapper.m_MainMenuActionsCallbackInterface.OnReset;
            }
            m_Wrapper.m_MainMenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Up.started += instance.OnUp;
                @Up.performed += instance.OnUp;
                @Up.canceled += instance.OnUp;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
                @Confirm.started += instance.OnConfirm;
                @Confirm.performed += instance.OnConfirm;
                @Confirm.canceled += instance.OnConfirm;
                @Reset.started += instance.OnReset;
                @Reset.performed += instance.OnReset;
                @Reset.canceled += instance.OnReset;
            }
        }
    }
    public MainMenuActions @MainMenu => new MainMenuActions(this);
    public interface IGameplayActions
    {
        void OnForward(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnStart(InputAction.CallbackContext context);
        void OnPreviousCar(InputAction.CallbackContext context);
        void OnNextCar(InputAction.CallbackContext context);
        void OnTurn(InputAction.CallbackContext context);
        void OnGas(InputAction.CallbackContext context);
        void OnUndo(InputAction.CallbackContext context);
        void OnRestart(InputAction.CallbackContext context);
        void OnExit(InputAction.CallbackContext context);
    }
    public interface IBranchPanelActions
    {
        void OnLeft(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnConfirm(InputAction.CallbackContext context);
    }
    public interface IDialoguePanelActions
    {
        void OnConfirm(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
    }
    public interface IMainMenuActions
    {
        void OnUp(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnConfirm(InputAction.CallbackContext context);
        void OnReset(InputAction.CallbackContext context);
    }
}
