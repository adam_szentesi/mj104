using UnityEngine;

public class ButtonElement : TriggerElement
{
  [SerializeField]
  private SphereCollider Collider;
  protected override Collider MyCollider => Collider;

  public override void PrepareForPush()
  {
    base.PrepareForPush();

    Collider.radius = 0.05f;
  }

  private void OnTriggerEnter(Collider other)
  {
    Domino domino = other.GetComponent<Domino>();

    if (domino)
    {
      Trigger();
    }

    Car car = other.GetComponent<Car>();

    if (car)
    {
      Game.Instance.StopCar(car);
    }
  }

}
