using UnityEngine;

public abstract class Element : MonoBehaviour
{
  [SerializeField]
  protected GameObject Marker;

  [SerializeField]
  protected ElementIcon ElementIcon;

  [SerializeField]
  protected int Score = 0;

  protected virtual Collider MyCollider { get; }

  private void Awake()
  {
    if (MyCollider != null)
    {
      MyCollider.isTrigger = true;
    }

    Game.Instance.Register(this);

    gameObject.layer = Layers.ElementLayer;
    OnAwake();
  }

  protected virtual void OnAwake() { }

  private void OnDestroy()
  {
    Game.Instance.Unregister(this);
  }

  public virtual void PrepareForPush()
  {
    if (Marker != null)
    {
      Marker.SetActive(false);
    }

    if (ElementIcon != null)
    {
      ElementIcon.gameObject.SetActive(false);
    }

  }

  protected void AddScore()
  {
    Game.Instance.AddScore(Score);
  }

  public virtual void Activate() { }
  
}
