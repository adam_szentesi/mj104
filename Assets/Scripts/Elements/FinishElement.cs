using UnityEngine;

public class FinishElement : Element
{
  [SerializeField]
  private SphereCollider Collider;
  protected override Collider MyCollider => Collider;

  private bool Triggered = false;

  private void Start()
  {
    ElementIcon.SetText("F");
  }

  public override void PrepareForPush()
  {
    base.PrepareForPush();

    Collider.radius = 0.05f;
  }

  private void OnTriggerEnter(Collider other)
  {
    Domino domino = other.GetComponent<Domino>();

    if (domino && !Triggered)
    {
      AddScore();
      Triggered = true;
    }

    Car car = other.GetComponent<Car>();

    if (car)
    {
      Game.Instance.StopCar(car);
    }
  }

}
