using System;
using UnityEngine;

public abstract class DominoDeployer : ScriptableObject
{
  public void Register()
  {
    Game.Instance.Register(this);
  }

  public virtual void Init() {}
  public abstract int Deploy(Car car, Action SpawnDomino);

}
