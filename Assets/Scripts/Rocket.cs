using UnityEngine;

public class Rocket : MonoBehaviour
{
  [SerializeField]
  private float Acceleration = 1.0f;

  private float CurrentTime = 0.0f;
  private bool IsLaunched = false;

  public void Launch()
  {
    IsLaunched = true;
  }

  private void Update()
  {
    if (IsLaunched)
    {
      CurrentTime += Time.deltaTime;
      float distance = 0.5f * Acceleration * CurrentTime * CurrentTime;
      Vector3 translation = Vector3.up * distance;
      transform.Translate(translation, Space.Self);

      if (CurrentTime > 5.0f)
      {
        Destroy(gameObject);
      }
    }
  }

}
