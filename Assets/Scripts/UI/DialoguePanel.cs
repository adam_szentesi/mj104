using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI MessageText;

  [SerializeField]
  private Button CancelButton;

  [SerializeField]
  private Button ConfirmButton;

  private InputActions InputActions;

  private void Awake()
  {
    InputActions = new InputActions();
    InputActions.DialoguePanel.Cancel.performed += context => Cancel();
    InputActions.DialoguePanel.Confirm.performed += context => Confirm();
  }

  public void Show(string message, UnityAction onConfirm, UnityAction onCancel)
  {
    CancelButton.onClick.AddListener(onCancel);
    CancelButton.gameObject.SetActive(true);

    Show(message, onConfirm);
  }

  public void Show(string message, UnityAction onConfirm)
  {
    MessageText.text = message;

    ConfirmButton.onClick.AddListener(onConfirm);
    ConfirmButton.gameObject.SetActive(true);

    gameObject.SetActive(true);

    InputActions.DialoguePanel.Enable();
  }

  public void Hide()
  {
    CancelButton.onClick.RemoveAllListeners();
    ConfirmButton.onClick.RemoveAllListeners();

    CancelButton.gameObject.SetActive(false);
    ConfirmButton.gameObject.SetActive(false);
    gameObject.SetActive(false);
    InputActions?.DialoguePanel.Disable();
  }

  private void Cancel()
  {
    CancelButton.onClick.Invoke();
  }

  private void Confirm()
  {
    ConfirmButton.onClick.Invoke();
  }

}
