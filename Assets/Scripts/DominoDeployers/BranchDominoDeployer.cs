using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Domino/BranchDominoDeployer")]
public class BranchDominoDeployer : DominoDeployer
{
  [SerializeField]
  private float Offset = 0.01f;

  [SerializeField]
  private GameObject BranchPanelPrefab;

  private InputActions InputActions;
  private BranchPanel BranchPanel;
  private Car LeftCar;
  private Car RightCar;
  private Car SelectedCar;

  public override void Init()
  {
    InputActions = new InputActions();
    InputActions.BranchPanel.Left.performed += context => SelectCar(LeftCar);
    InputActions.BranchPanel.Right.performed += context => SelectCar(RightCar);
    InputActions.BranchPanel.Confirm.performed += context => Confirm();

    BranchPanel = Game.Instance.RegisterPanel<BranchPanel>(BranchPanelPrefab);
    BranchPanel.Hide();
  }

  public override int Deploy(Car car, Action SpawnDomino)
  {
    Vector3 position = car.transform.position;
    Vector3 offset = car.transform.right * Offset;
    Quaternion rotation = car.transform.rotation;

    Game.Instance.DespawnCar(car, false);

    LeftCar = Game.Instance.SpawnCar(position - offset, rotation);
    RightCar = Game.Instance.SpawnCar(position + offset, rotation);

    BranchPanel.UpdateTexts("Select car", "", "");
    Game.Instance.ShowPanel(BranchPanel);
    InputActions.BranchPanel.Enable();

    SelectedCar = null;

    return 0;
  }

  private void Confirm()
  {
    if (SelectedCar != null)
    {
      Game.Instance.HidePanel(BranchPanel);
      InputActions.BranchPanel.Disable();
    }
  }

  private void SelectCar(Car car)
  {
    if (car != null)
    {
      SelectedCar = car;
      Game.Instance.SetCurrentCar(car.Index, false);
    }
  }

}
