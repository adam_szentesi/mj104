using TMPro;
using UnityEngine;

public class ElementIcon : MonoBehaviour
{
  [SerializeField]
  private SpriteRenderer SpriteRenderer;

  [SerializeField]
  private TextMeshPro Text;

  void Update()
  {
    SpriteRenderer.transform.LookAt(Game.Instance.Camera.transform.position, Vector3.up);
  }

  public void SetText(string text)
  {
    Text.text = text;
  }

}
