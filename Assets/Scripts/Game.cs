using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class Game : MonoBehaviour
{
  [SerializeField]
  private GameObject BankPrefab;

  [SerializeField]
  private float PushForce = 0.001f;

  [SerializeField]
  private float _DeployDistance = 0.05f;
  public float DeployDistance => _DeployDistance;

  [SerializeField]
  private MainPanel MainPanel;
  
  [SerializeField]
  private ScorePanel ScorePanel;
  
  [SerializeField]
  private DominoStack DominoStack;

  [SerializeField]
  private int DominoLoadInitial = 10;

  [SerializeField]
  private int TargetScore = 0;

  [SerializeField]
  private Camera MainCamera;

  [SerializeField]
  private Transform OverheadCameraPosition;

  [SerializeField]
  private PushCamera PushCamera;

  [SerializeField]
  private float CameraLerp = 0.01f;

  [SerializeField]
  private AudioClip DominoAudioClip;

  [SerializeField]
  private AudioSource DominoAudioSource;

  [SerializeField]
  private float DominoAudioInterval = 0.1f;

  [Header("UI")]
  [SerializeField]
  private Canvas Canvas;
  
  public int DominoLeft => DominoStack.DominoLeft;
  public bool CanDeploy => DominoLeft > 0;
  public Camera Camera => MainCamera;

  public static Game Instance { get; private set; }

  private int CurrentCarIndex;
  private Car CurrentCar => Cars[CurrentCarIndex];
  private List<Car> Cars = new List<Car>();
  private List<int> InactiveCarIndices = new List<int>();
  private List<Element> Elements = new List<Element>();
  private List<Domino> Dominos = new List<Domino>();
  private HashSet<DominoDeployer> DominoDeployers = new HashSet<DominoDeployer>();
  private InputActions InputActions;
  private float ForwardValue = 0.0f;
  private float RightValue = 0.0f;
  private float TurnValue = 0.0f;
  private float GasValue = 0.0f;
  private Domino FirstDomino => Dominos[0];
  private int DominoCounter = 0;
  private bool IsAnyActiveCar => Cars.Count > InactiveCarIndices.Count;
  private Transform CameraTargetPosition;
  private StartElement StartElement;
  private float DominoAudioTimer = 0.0f;
  private int UnclaimedDominoesCount = 0;
  private bool IsReadyForPush = false;
  private int Score = 0;

  private void Awake()
  {
    if (Instance)
    {
      Debug.Log("DESTROY");
      Destroy(this);
      return;
    }

    Instance = this;

    if (Bank.Instance == null)
    {
      Bank bank = Instantiate(BankPrefab).GetComponent<Bank>();
    }

    InputActions = new InputActions();
    InputActions.Gameplay.Enable();
    InputActions.Gameplay.Forward.performed += context => { ForwardValue = context.ReadValue<float>(); };
    InputActions.Gameplay.Forward.canceled += context => { ForwardValue = 0.0f; };
    InputActions.Gameplay.Right.performed += context => { RightValue = context.ReadValue<float>(); };
    InputActions.Gameplay.Right.canceled += context => { RightValue = 0.0f; };
    InputActions.Gameplay.Turn.performed += context => { TurnValue = context.ReadValue<float>(); };
    InputActions.Gameplay.Turn.canceled += context => { TurnValue = 0.0f; };
    InputActions.Gameplay.Gas.performed += context => { GasValue = context.ReadValue<float>(); };
    InputActions.Gameplay.Gas.canceled += context => { GasValue = 0.0f; };
    InputActions.Gameplay.Start.performed += context => PushFirstDomino();
    InputActions.Gameplay.PreviousCar.performed += context => PreviousCar(false);
    InputActions.Gameplay.NextCar.performed += context => NextCar(false);
    InputActions.Gameplay.Undo.performed += context => CurrentCar.Undo();
    InputActions.Gameplay.Restart.performed += context => Restart();
    InputActions.Gameplay.Exit.performed += context => Exit();

    Material material = new Material(Bank.Instance.DominoPrefab.GetComponent<Domino>().GetSharedMaterial());
    material.color = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 1.0f, 1.0f);
    PopulateDominoStack(DominoLoadInitial, material);

    UpdateMainPanel();
  }

  private void Restart()
  {
    ShowDialoguePanel("Restart level?", () => Bank.Instance.LoadCurrentLevel(), () => HideDialoguePanel());
  }

  private void Exit()
  {
    ShowDialoguePanel("Return to menu?", () => Bank.Instance.LoadMainMenu(), () => HideDialoguePanel());
  }

  private void Start()
  {
    ResetCameraTargetPosition();

    foreach (DominoDeployer dominoDeployer in DominoDeployers)
    {
      dominoDeployer.Init();
    }

    Car car = SpawnCar(StartElement.transform.position, StartElement.transform.rotation);
    SetCurrentCar(car.Index, false);
  }

  private void ResetCameraTargetPosition()
  {
    PushCamera.transform.position = StartElement.transform.position;
    PushCamera.transform.rotation = StartElement.transform.rotation;

    CameraTargetPosition = PushCamera.CameraPosition;
  }

  private void Update()
  {
    if (CurrentCar.IsActive)
    {
      CurrentCar.Move(GasValue, RightValue, TurnValue);
    }
    else
    {
      PushCamera.Move(ForwardValue, RightValue, TurnValue);
    }

    MainCamera.transform.position = Vector3.Lerp(MainCamera.transform.position, CameraTargetPosition.position, CameraLerp);
    MainCamera.transform.rotation = Quaternion.Slerp(MainCamera.transform.rotation, CameraTargetPosition.rotation, CameraLerp);

    DominoAudioTimer -= Time.deltaTime;
    if (DominoAudioTimer < 0.0)
    {
      DominoAudioTimer = 0.0f;
    }
  }

  public void PlayDominoAudio(Vector3 position)
  {
    if (DominoAudioTimer == 0.0f)
    {
      DominoAudioSource.transform.position = position;
      DominoAudioSource.volume = Random.Range(0.7f, 1.0f);
      DominoAudioSource.pitch = Random.Range(0.95f, 1.05f);
      DominoAudioSource.clip = DominoAudioClip;
      DominoAudioSource.Play();
      DominoAudioTimer = DominoAudioInterval;
    }
  }

  private void OnDeploy(int incement)
  {
    DominoCounter += incement;
  }

  private void UpdateMainPanel()
  {
    MainPanel.UpdateText(UnclaimedDominoesCount, DominoStack.DominoLeft);
    ScorePanel.SetTargetScore(TargetScore);
    ScorePanel.SetScore(Score);
  }

  public void AddScore(int score)
  {
    Score += score;
    UpdateMainPanel();

    if (Score >= TargetScore)
    {
      ShowDialoguePanel("Next level?", NextLevel, HideDialoguePanel);
    }
  }

  private void NextLevel()
  {
    HideDialoguePanel();
    StartCoroutine(NextCor());
  }

  private IEnumerator NextCor()
  {
    yield return null;
    yield return null;

    Bank.Instance.LoadNextLevel();
  }

  private bool PrepareForPush()
  {
    if (IsReadyForPush)
    {
      return true;
    }

    if (Dominos.Count == 0)
    {
      ShowDialoguePanel("There are no dominoes on playground.", HideDialoguePanel);
      return false;
    }

    foreach (Car car in Cars)
    {
      DespawnCar(car, false);
    }

    foreach (Element element in Elements)
    {
      element.PrepareForPush();
    }

    foreach (Domino domino in Dominos)
    {
      domino.PrepareForPush();
    }

    ResetCameraTargetPosition();

    IsReadyForPush = true;
    return true;
  }

  private void PushFirstDomino()
  {
    if (!PrepareForPush())
    {
      ;return;
    }

    FirstDomino.Push(PushForce);
  }

  public Car SpawnCar(Vector3 position, Quaternion rotation)
  {
    Car car;

    if (InactiveCarIndices.Count == 0)
    {
      int index = Cars.Count;
      car = Instantiate(Bank.Instance.CarPrefab, position, rotation).GetComponent<Car>();
      car.Init(index, OnDeploy);
      Cars.Add(car);
    }
    else
    {
      int lastMetaIndex = InactiveCarIndices.Count - 1;
      int lastIndex = InactiveCarIndices[lastMetaIndex];
      InactiveCarIndices.RemoveAt(lastMetaIndex);
      car = Cars[lastIndex];
      car.gameObject.transform.position = position;
      car.gameObject.transform.rotation = rotation;
    }

    car.Activate();
    car.ResetCar();

    return car;
  }

  public void DespawnCar(Car car, bool switchToNextCar = true)
  {
    car.gameObject.SetActive(false);

    if (car.IsActive)
    {
      DeactivateCar(car, switchToNextCar);
    }
  }

  public void DeactivateCar(Car car, bool switchToNextCar = true)
  {
    car.Deactivate();
    InactiveCarIndices.Add(car.Index);

    if (switchToNextCar)
    {
      if (!NextCar())
      {
        PrepareForPush();
      }
    }
  }

  public void StopCar(Car car)
  {
    car.CanMove = false;
  }

  public void SetCurrentCar(int index, bool showDialogue = true)
  {
    Car car = Cars[index];

    if (car != null && car.IsActive)
    {
      CameraTargetPosition = car.CameraPosition;
      CurrentCarIndex = index;

      DominoStack.SetPosition(CurrentCar.MyStackPosition);

      if (showDialogue)
      {
        ShowDialoguePanel("Car switched.", HideDialoguePanel);
      }
    }
  }

  public void Register(Element element)
  {
    Elements.Add(element);

    StartElement start = element as StartElement;
    
    if (start != null)
    {
      StartElement = start;
    }
  }

  public void Unregister(Element element)
  {
    Elements.Remove(element);
  }

  public void Register(Domino domino)
  {
    Dominos.Add(domino);
  }

  public void Unregister(Domino domino)
  {
    Dominos.Remove(domino);
  }

  public void Register(DominoDeployer dominoDeployer)
  {
    if (!DominoDeployers.Contains(dominoDeployer))
    {
      DominoDeployers.Add(dominoDeployer);
    }
  }

  private void PreviousCar(bool showDialogue = true)
  {
    if (!IsAnyActiveCar)
    {
      return;
    }

    int currentCarIndex = CurrentCarIndex;

    for (int i = 0; i < Cars.Count - 1; i++)
    {
      currentCarIndex--;

      if (currentCarIndex < 0)
      {
        currentCarIndex += Cars.Count;
      }

      if (Cars[currentCarIndex].IsActive)
      {
        SetCurrentCar(currentCarIndex, showDialogue);
        break;
      }
    }
  }

  private bool NextCar(bool showDialogue = true)
  {
    if (!IsAnyActiveCar || DominoLeft == 0)
    {
      return false;
    }

    int currentCarIndex = CurrentCarIndex;

    for (int i = 0; i < Cars.Count; i++)
    {
      currentCarIndex++;

      if (currentCarIndex >= Cars.Count)
      {
        currentCarIndex -= Cars.Count;
      }
      
      if (Cars[currentCarIndex].IsActive)
      {
        SetCurrentCar(currentCarIndex, showDialogue);
        break;
      }
    }

    return true;
  }

  private void DisableGameplayInput()
  {
    InputActions.Gameplay.Disable();
  }

  private void EnableGameplayInput()
  {
    InputActions.Gameplay.Enable();
  }

  public void AddRefillDominos(int count)
  {
    UnclaimedDominoesCount += count;
}

  // Domino stack

  public void PopulateDominoStack(int count, Material material)
  {
    DominoStack.PopulateDominoStack(count, material);
  }

  public Domino DequeueDomino()
  {
    Domino result = DominoStack.DequeueDomino();
    Score--;
    UpdateMainPanel();
    return result;
  }

  public void EnqueueTopDomino(Domino domino)
  {
    DominoStack.EnqueueTopDomino(domino);
    Score++;
    UpdateMainPanel();
  }

  public List<Domino> DominoStackList => DominoStack.List;

  // UI

  public T RegisterPanel<T>(GameObject panelPrefab) where T : Panel
  {
    return Instantiate(panelPrefab, Canvas.transform).GetComponent<T>();
  }

  public void ShowPanel(Panel panel)
  {
    DisableGameplayInput();
    panel.Show();
  }

  public void HidePanel(Panel panel)
  {
    EnableGameplayInput();
    panel.Hide();
  }

  public void ShowDialoguePanel(string message, UnityAction onConfirm)
  {
    DisableGameplayInput();
    Bank.Instance.ShowDialoguePanel(message, onConfirm);
  }
  
  public void ShowDialoguePanel(string message, UnityAction onConfirm, UnityAction onCancel)
  {
    DisableGameplayInput();
    Bank.Instance.ShowDialoguePanel(message, onConfirm, onCancel);
  }

  public void HideDialoguePanel()
  {
    EnableGameplayInput();
    Bank.Instance.HideDialoguePanel();
  }

  private void OnDestroy()
  {
    InputActions.Dispose();
  }

}
