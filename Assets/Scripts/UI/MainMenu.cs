using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
  [SerializeField]
  private Transform CameraPivot;

  [SerializeField]
  private int DominoCount = 100;

  [SerializeField]
  private float TurnRate = 1.0f;

  [SerializeField]
  private GameObject MainPanel;

  [SerializeField]
  private GameObject InfoPanel;

  [SerializeField]
  private MenuButton[] MenuButtons = new MenuButton[] { };

  private MenuButton CurrentMenuButton;
  private int CurrentButtonIndex = 0;
  private InputActions InputActions;

  private void Awake()
  {
    InputActions = new InputActions();
    InputActions.MainMenu.Enable();
    InputActions.MainMenu.Up.performed += context => UpPerformed();
    InputActions.MainMenu.Down.performed += context => DownPerformed();
    InputActions.MainMenu.Confirm.performed += context => ConfirmPerformed();
    InputActions.MainMenu.Reset.performed += context => Bank.Instance.Reset();

    InfoPanel.SetActive(false);
    MainPanel.SetActive(true);
  }

  private void Start()
  {
    UpdateCurrentMenuButton();

    for (int i = 0; i < DominoCount; i++)
    {
      Vector3 position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
      Quaternion rotation = Quaternion.Euler(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f));

      Domino domino = Instantiate(Bank.Instance.DominoPrefab, position, rotation).GetComponent<Domino>();
      Material material = domino.GetMaterial();
      material.color = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 1.0f, 1.0f);
    }

    Bank.Instance.HideDialoguePanel();
  }

  private void Update()
  {
    CameraPivot.Rotate(new Vector3(0.0f, TurnRate * Time.deltaTime, 0.0f), Space.Self);
  }

  private void UpPerformed()
  {
    CurrentButtonIndex--;

    if (CurrentButtonIndex < 0)
    {
      CurrentButtonIndex = 0;
    }

    UpdateCurrentMenuButton();
  }

  private void DownPerformed()
  {
    CurrentButtonIndex++;

    if (CurrentButtonIndex > MenuButtons.Length - 1)
    {
      CurrentButtonIndex = MenuButtons.Length - 1;
    }

    UpdateCurrentMenuButton();
  }

  private void UpdateCurrentMenuButton()
  {
    if (CurrentMenuButton != null)
    {
      CurrentMenuButton.Toggle(false);
    }

    CurrentMenuButton = MenuButtons[CurrentButtonIndex];
    CurrentMenuButton.Toggle(true);
  }

  private void ConfirmPerformed()
  {
    CurrentMenuButton.Click();
  }

  public void StartClicked()
  {
    Bank.Instance.LoadCurrentLevel();
  }

  public void QuitClicked()
  {
    Application.Quit();
  }

  public void ToggleInfo()
  {
    bool state = InfoPanel.activeInHierarchy;

    InfoPanel.SetActive(!state);
    MainPanel.SetActive(state);

    if (state)
    {
      InputActions.MainMenu.Up.Enable();
      InputActions.MainMenu.Down.Enable();
      InputActions.MainMenu.Reset.Enable();

      //InputActions.MainMenu.Confirm.performed += context => ConfirmPerformed();
    }
    else
    {
      InputActions.MainMenu.Up.Disable();
      InputActions.MainMenu.Down.Disable();
      InputActions.MainMenu.Reset.Disable();

      //InputActions.MainMenu.Confirm.performed += context => ToggleInfo();
    }
  }

  private void OnDestroy()
  {
    InputActions.Dispose();
  }

}
