using UnityEngine;

public class Domino : MonoBehaviour
{
  [SerializeField]
  private Rigidbody Rigidbody;

  [SerializeField]
  private MeshRenderer MeshRenderer;

  [SerializeField]
  private GameObject _Visuals;
  public GameObject Visuals => _Visuals;

  public float VelocitySquared => Rigidbody.velocity.sqrMagnitude;
  public float AngularSquared => Rigidbody.angularVelocity.sqrMagnitude;
  public bool IsAsleep => Rigidbody.IsSleeping();

  public Vector3 LocalPosition
  {
    get
    {
      return gameObject.transform.localPosition;
    }
    set
    {
      gameObject.transform.localPosition = value;
    }
  }

  public Quaternion LocalRotation
  {
    get
    {
      return gameObject.transform.localRotation;
    }
    set
    {
      gameObject.transform.localRotation = value;
    }
  }

  public const float Thickness = 0.01f;
  public const float Height = 0.06f;

  private void Awake()
  {
    gameObject.layer = Layers.DefaultLayer;
  }

  public void Init(Vector3 position, Quaternion rotation)
  {
    Init(position, rotation, name);
  }

  public void Init(Vector3 position, Quaternion rotation, string name)
  {
    gameObject.transform.SetParent(null);
    gameObject.transform.position = position;
    gameObject.transform.rotation = rotation;
    gameObject.name = name;
  }

  public void Push(float force)
  {
    Vector3 forceVector = transform.forward * force;
    Vector3 forcePosition = new Vector3(0.0f, 0.5f, 0.0f);
    Rigidbody.AddForceAtPosition(forceVector, forcePosition, ForceMode.Impulse);
  }

  public void PrepareForPush()
  {
    gameObject.layer = Layers.DominoLayer;
    Rigidbody.isKinematic = false;
  }

  public Material GetMaterial()
  {
    return MeshRenderer.material;
  }

  public void SetMaterial(Material material)
  {
    MeshRenderer.material = material;
  }

  public Material GetSharedMaterial()
  {
    return MeshRenderer.sharedMaterial;
  }

  public void RotateAround(Vector3 pivot, float angle)
  {
    transform.RotateAround(pivot, Vector3.up, angle);
  }

  public void SelfDestruct()
  {
    Destroy(gameObject);
  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.rigidbody == null)
    {
      return;
    }

    if (collision.rigidbody.GetComponent<Domino>() != null)
    {
      Game.Instance.PlayDominoAudio(transform.position);
    }
    
  }

}
