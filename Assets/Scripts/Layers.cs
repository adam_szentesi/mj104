using UnityEngine;

public static class Layers
{
  public const string LevelLayerName = "Level";
  public const string DefaultLayerName = "Default";
  public const string ElementLayerName = "Element";
  public const string DominoLayerName = "Domino";
  public const string CarLayerName = "Car";

  public static int LevelLayer => LayerMask.NameToLayer(LevelLayerName);
  public static int DefaultLayer => LayerMask.NameToLayer(DefaultLayerName);
  public static int ElementLayer => LayerMask.NameToLayer(ElementLayerName);
  public static int DominoLayer => LayerMask.NameToLayer(DominoLayerName);
  public static int CarLayer => LayerMask.NameToLayer(CarLayerName);

}
