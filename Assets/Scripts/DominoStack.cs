using System.Collections.Generic;
using UnityEngine;

public class DominoStack : MonoBehaviour
{
  private SuperQueue<Domino> DominoQueue = new SuperQueue<Domino>();

  public int DominoLeft => DominoQueue.Count;
  public List<Domino> List => DominoQueue.List;

  public Domino DequeueDomino()
  {
    foreach (Domino domino in List)
    {
      Vector3 stackPosition = domino.LocalPosition;
      stackPosition.y -= Domino.Thickness;
      domino.LocalPosition = stackPosition;
    }

    return DominoQueue.Dequeue();
  }

  public void EnqueueTopDomino(Domino domino)
  {
    int index = DominoQueue.Count;
    domino.gameObject.transform.SetParent(transform);
    domino.LocalPosition = new Vector3(0.0f, index * Domino.Thickness + Domino.Thickness * 0.5f, -Domino.Height * 0.5f);
    domino.LocalRotation = Quaternion.Euler(-90.0f, 0.0f, 180.0f);
    domino.RotateAround(transform.position, UnityEngine.Random.Range(-4.0f, 4.0f));
    DominoQueue.Enqueue(domino);
  }

  public void Clear() => DominoQueue.Clear();

  public void PopulateDominoStack(int count, Material material)
  {
    int dominoLeft = DominoLeft;

    for (int i = dominoLeft; i < dominoLeft + count; i++)
    {
      GameObject dominoGO = Instantiate(Bank.Instance.DominoPrefab, transform, false);
      Domino domino = dominoGO.GetComponent<Domino>();
      domino.Init(transform.position, transform.rotation, "Domino" + i);
      domino.SetMaterial(material);
      EnqueueTopDomino(domino);
    }
  }

  public void SetPosition(Transform parent)
  {
    transform.SetParent(parent);
    transform.localPosition = Vector3.zero;
    transform.localRotation = Quaternion.identity;
  }

}
