using UnityEngine;

public class RocketElement : Element
{
  [SerializeField]
  private Rocket Rocket;

  public override void Activate()
  {
    if (Rocket != null)
    {
      Rocket.transform.SetParent(null, true);
      Rocket.Launch();
      AddScore();
    }
  }

}
