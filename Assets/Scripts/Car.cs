using System;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
  [SerializeField]
  private float Speed = 1.0f;

  [SerializeField]
  private float TurnRate = 90.0f;

  [SerializeField]
  private PushCamera PushCamera;

  public Transform CameraPosition => PushCamera.CameraPosition;

  [SerializeField]
  private Transform StackPosition;
  public Transform MyStackPosition => StackPosition;

  [SerializeField]
  private DominoDeployer DefaultDominoDeployer;

  [SerializeField]
  private AudioSource DeployAudioSource;

  [SerializeField]
  private AudioClip PutAudioClip;

  [SerializeField]
  private AudioClip PickAudioClip;

  [SerializeField]
  private MeshRenderer MeshRenderer;

  [SerializeField]
  private CarSensor CarSensor;

  public int Index { get; private set; } = -1;
  public bool IsActive { get; private set; } = false;
  public Domino LastDeployedDomino => DeployedDominos.Peek();
  public bool CanMove = true;

  private float DistanceCounter = 0.0f;
  private Action<int> OnDeploy;
  private Stack<DominoDeployer> DominoDeployers = new Stack<DominoDeployer>();
  
  private Stack<Domino> DeployedDominos = new Stack<Domino>();

  private void Awake()
  {
    gameObject.layer = Layers.CarLayer;

    CarSensor.OnTrigger = OnTrigger;
  }

  public void Init(int index, Action<int> onDeploy)
  {
    Index = index;
    OnDeploy = onDeploy;
    gameObject.name = "Car " + Index;

    MeshRenderer.material.color = Color.HSVToRGB(UnityEngine.Random.Range(0.0f, 1.0f), 1.0f, 1.0f);
  }

  public void Activate()
  {
    IsActive = true;
    gameObject.SetActive(IsActive);
  }

  public void Deactivate()
  {
    IsActive = false;
    gameObject.SetActive(IsActive);
  }

  public void Move(float forwardValue, float rightValue, float turnValue)
  {
    PushCamera.Turn(turnValue);

    if (forwardValue <= 0.0f || !CanMove || !Game.Instance.CanDeploy)
    {
      return;
    }

    float distance = Speed * forwardValue * Time.deltaTime;
    float angle = TurnRate * rightValue * Time.deltaTime * forwardValue;

    DistanceCounter += distance;

    if (DistanceCounter >= Game.Instance.DeployDistance)
    {
      DistanceCounter -= Game.Instance.DeployDistance;
      Deploy();
    }

    transform.Rotate(new Vector3(0.0f, angle, 0.0f), Space.Self);
    transform.Translate(transform.forward * distance, Space.World);
  }

  public void Deploy()
  {
    if (!Game.Instance.CanDeploy)
    {
      return;
    }

    int spawnCount = PopDominoDeployer().Deploy(this, SpawnDomino);
    OnDeploy.Invoke(spawnCount);

    if (!Game.Instance.CanDeploy)
    {
      Game.Instance.StopCar(this);
    }
  }

  private DominoDeployer PopDominoDeployer()
  {
    return DominoDeployers.Count > 0 ? DominoDeployers.Pop() : DefaultDominoDeployer;
  }

  private void SpawnDomino()
  {
    DeployedDominos.Push(Game.Instance.DequeueDomino());
    LastDeployedDomino.Init(transform.position, transform.rotation);
    Game.Instance.Register(LastDeployedDomino);

    DeployAudioSource.clip = PutAudioClip;
    DeployAudioSource.volume = UnityEngine.Random.Range(0.7f, 1.0f);
    DeployAudioSource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
    DeployAudioSource.Play();
  }

  public void StackDominoDeployer(DominoDeployer dominoDeployer)
  {
    DominoDeployers.Push(dominoDeployer);
  }

  public void Undo()
  {
    if (DeployedDominos.Count == 0 || !IsActive)
    {
      return;
    }

    DeployAudioSource.clip = PickAudioClip;
    DeployAudioSource.volume = UnityEngine.Random.Range(0.7f, 1.0f);
    DeployAudioSource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
    DeployAudioSource.Play();

    Domino lastDomino = DeployedDominos.Pop();
    transform.position = lastDomino.transform.position;
    Vector3 rotation = lastDomino.transform.rotation.eulerAngles;
    rotation.x = 0.0f;
    rotation.z = 0.0f;
    transform.eulerAngles = rotation;
    Game.Instance.EnqueueTopDomino(lastDomino);
    DistanceCounter = Game.Instance.DeployDistance;
    Game.Instance.Unregister(lastDomino);
    CanMove = true;
  }

  public void ResetCar()
  {
    DistanceCounter = Game.Instance.DeployDistance;
    DominoDeployers.Clear();
    DeployedDominos.Clear();
    PushCamera.Reset();
  }

  private void OnTrigger()
  {
    Debug.Log("SENSOR TRIGGER");
    CanMove = false;
  }

}
