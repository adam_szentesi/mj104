using TMPro;
using UnityEngine;

public class MainPanel : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI UnclaimedDominoesCountText;

  [SerializeField]
  private TextMeshProUGUI LoadedDominoesCountText;

  public void UpdateText(int unclaimedDominoesCount, int loadedDominoesCount)
  {
    UnclaimedDominoesCountText.text = "Unclaimed dominoes: " + unclaimedDominoesCount.ToString();
    LoadedDominoesCountText.text = "Loaded dominoes: " + loadedDominoesCount.ToString();
  }

}
