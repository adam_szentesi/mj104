using TMPro;
using UnityEngine;

public class ScorePanel : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI TargetScoreText;

  [SerializeField]
  private TextMeshProUGUI ScoreText;

  public void SetTargetScore(int score)
  {
    TargetScoreText.text = "Target score: " + score.ToString();
  }

  public void SetScore(int score)
  {
    ScoreText.text = "Score: " + score.ToString();
  }

}
