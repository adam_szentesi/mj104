using UnityEngine;

public class PushCamera : MonoBehaviour
{
  [SerializeField]
  private float Speed = 1.0f;

  [SerializeField]
  private float TurnRate = 90.0f;

  [SerializeField]
  private Transform _CameraPosition;
  public Transform CameraPosition => _CameraPosition;

  public void Move(float forwardValue, float rightValue, float turnValue)
  {
    float forward = Speed * forwardValue * Time.deltaTime;
    float right = Speed * rightValue * Time.deltaTime;

    Turn(turnValue);
    
    transform.Translate(transform.forward * forward + transform.right * right, Space.World);
  }

  public void Turn(float turnValue)
  {
    float angle = TurnRate * turnValue * Time.deltaTime;

    transform.Rotate(new Vector3(0.0f, angle, 0.0f), Space.Self);
  }

  public void Reset()
  {
    transform.localRotation = Quaternion.identity;
  }

}
