using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class MenuButtonClickedEvent : UnityEvent
{
}

public class MenuButton : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI Text;

  [SerializeField]
  private MenuButtonClickedEvent OnClick = new MenuButtonClickedEvent();

  private void Awake()
  {
    Toggle(false);
  }

  public void Toggle(bool state)
  {
    Color color = Text.color;
    color.a = state ? 1.0f : 0.5f;
    Text.color = color;
  }

  public void Click()
  {
    OnClick.Invoke();
  }

}
