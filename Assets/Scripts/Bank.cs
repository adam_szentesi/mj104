using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Bank : MonoBehaviour
{
  public const string LevelIndexSaveString = "LevelIndex";

  [SerializeField]
  private DialoguePanel DialoguePanel;

  [SerializeField]
  private GameObject _DominoPrefab;
  public GameObject DominoPrefab => _DominoPrefab;

  [SerializeField]
  private GameObject _CarPrefab;
  public GameObject CarPrefab => _CarPrefab;

  [SerializeField]
  private int[] Levels = new int[] { };

  [SerializeField]
  private int CurrentLevelIndex = 0;

  public static Bank Instance { get; private set; }

  private void Awake()
  {
    if (Instance)
    {
      Destroy(this);
      return;
    }

    Instance = this;

    DontDestroyOnLoad(gameObject);

    CurrentLevelIndex = PlayerPrefs.GetInt(LevelIndexSaveString, 0);
  }

  public void LoadNextLevel()
  {
    PlayerPrefs.SetInt(LevelIndexSaveString, CurrentLevelIndex);

    if (CurrentLevelIndex < Levels.Length - 1)
    {
      CurrentLevelIndex++;
      LoadCurrentLevel();
    }
    else
    {
      ShowDialoguePanel("This one was last!", () => LoadMainMenu());
    }
  }

  public void LoadCurrentLevel()
  {
    HideDialoguePanel();
    SceneManager.LoadScene(Levels[CurrentLevelIndex]);
  }

  public void LoadMainMenu()
  {
    SceneManager.LoadScene(0);
  }

  public void Reset()
  {
    PlayerPrefs.SetInt(LevelIndexSaveString, 0);
    CurrentLevelIndex = PlayerPrefs.GetInt(LevelIndexSaveString, 0);
  }

  public void ShowDialoguePanel(string message, UnityAction onConfirm)
  {
    DialoguePanel.Show(message, onConfirm);
  }

  public void ShowDialoguePanel(string message, UnityAction onConfirm, UnityAction onCancel)
  {
    DialoguePanel.Show(message, onConfirm, onCancel);
  }

  public void HideDialoguePanel()
  {
    DialoguePanel.Hide();
  }

}
