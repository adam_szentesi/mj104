using UnityEngine;

public class BrancherElement : Element
{
  [SerializeField]
  private DominoDeployer DominoDeployer;

  [SerializeField]
  private SphereCollider Collider;
  protected override Collider MyCollider => Collider;

  protected override void OnAwake()
  {
    DominoDeployer.Register();
  }

  private void OnTriggerEnter(Collider other)
  {
    Car car = other.GetComponent<Car>();

    if (car)
    {
      car.StackDominoDeployer(DominoDeployer);
      Destroy(gameObject);
    }
  }

}
