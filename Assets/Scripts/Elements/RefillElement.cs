using UnityEngine;

public class RefillElement : Element
{
  [SerializeField]
  private int RefilValue = 10;

  [SerializeField]
  private SphereCollider Collider;
  protected override Collider MyCollider => Collider;

  private Material Material;

  private void Start()
  {
    Game.Instance.AddRefillDominos(RefilValue);

    Material = new Material(Bank.Instance.DominoPrefab.GetComponent<Domino>().GetSharedMaterial());
    Material.color = Color.HSVToRGB(UnityEngine.Random.Range(0.0f, 1.0f), 1.0f, 1.0f);

    for (int i = 0; i < RefilValue; i++)
    {
      GameObject dominoGO = Instantiate(Bank.Instance.DominoPrefab, transform, false);
      Domino domino = dominoGO.GetComponent<Domino>();
      domino.LocalPosition = new Vector3(0.0f, i * Domino.Thickness + Domino.Thickness * 0.5f, -Domino.Height * 0.5f);
      domino.LocalRotation = Quaternion.Euler(-90.0f, 0.0f, 180.0f);
      domino.RotateAround(transform.position, UnityEngine.Random.Range(-4.0f, 4.0f));
      domino.SetMaterial(Material);
    }

    ElementIcon.SetText(RefilValue.ToString());
  }

  private void OnTriggerEnter(Collider other)
  {
    Car car = other.GetComponent<Car>();

    if (car)
    {
      Game.Instance.AddRefillDominos(-RefilValue);
      Game.Instance.PopulateDominoStack(RefilValue, Material);
      Destroy(gameObject);
    }
  }

}
