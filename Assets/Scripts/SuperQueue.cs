using System.Collections.Generic;

public class SuperQueue<T>
{
  public List<T> List = new List<T>();
  public int Count => List.Count;

  public void Enqueue(T element)
  {
    List.Add(element);
  }

  public T Dequeue()
  {
    T result = List[0];

    for (int i = 1; i < List.Count; i++)
    {
      List[i - 1] = List[i];
    }

    List.RemoveAt(List.Count - 1);
    return result;
  }

  public T DequeueLast()
  {
    int index = List.Count - 1;
    T result = List[index];
    List.RemoveAt(index);
    return result;
  }

  public void Clear()
  {
    List.Clear();
  }

}