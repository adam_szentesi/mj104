using System;
using UnityEngine;

public class CarSensor : MonoBehaviour
{
  public Action OnTrigger;

  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.layer == Layers.LevelLayer)
    {
      if (OnTrigger != null)
      {
        OnTrigger.Invoke();
      }
    }
  }

}
