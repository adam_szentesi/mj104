using TMPro;
using UnityEngine;

public class BranchPanel : Panel
{
  [SerializeField]
  protected TextMeshProUGUI LabelText;

  [SerializeField]
  protected TextMeshProUGUI DominosLeftText;

  [SerializeField]
  protected TextMeshProUGUI DominosRightText;

  public void UpdateTexts(string label, string leftText, string rightText)
  {
    LabelText.text = label;

    UpdateTexts(leftText, rightText);
  }

  public void UpdateTexts(string leftText, string rightText)
  {
    DominosLeftText.text = leftText;
    DominosRightText.text = rightText;
  }

}
