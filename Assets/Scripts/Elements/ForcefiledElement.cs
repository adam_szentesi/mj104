using UnityEngine;

public class ForcefiledElement : Element
{
  [SerializeField]
  private Transform Endpoint;

  [SerializeField]
  private float Width = 0.5f;

  [SerializeField]
  private BoxCollider Collider;
  protected override Collider MyCollider => Collider;

  [SerializeField]
  private Transform Forcefield;

  [SerializeField]
  private AudioSource AudioSource;

  private bool Triggered = false;

  private void Start()
  {
    Collider.enabled = false;
  }

  public override void PrepareForPush()
  {
    base.PrepareForPush();
    
    Collider.enabled = true;
  }

  public override void Activate()
  {
    if (!Triggered)
    {
      Forcefield.gameObject.SetActive(false);
      Collider.enabled = false;
      AddScore();
      Triggered = true;
    }
  }

  private void OnTriggerStay(Collider other)
  {
    Domino domino = other.GetComponent<Domino>();

    if (domino && (domino.VelocitySquared > 0.001f || domino.AngularSquared > 0.01f))
    {
      domino.SelfDestruct();
      AudioSource.Play();
    }
  }

  private void OnDrawGizmos()
  {
    Gizmos.color = new Color(1, 0, 0, 1);
    Gizmos.DrawLine(transform.position, Endpoint.position);
  }

  private void OnValidate()
  {
    Endpoint.transform.localPosition = transform.right * Width;

    Vector3 center = Collider.center;
    center.x = Width * 0.5f;
    Collider.center = center;

    Vector3 size = Collider.size;
    size.x = Width;
    Collider.size = size;

    Forcefield.localPosition = center;
    size.z = 1.0f;
    Forcefield.localScale = size;
  }

}
